# Relational/Document DB Equivalence

The purpose of this project is to convert my POGIL-like activities for CS-343 Software Construction, Design, and Architecture to a document DB.

The running example in my activities is an ordering system. The ordering system consists of:
1. A backing store (currently an in-memory database implemented as 3 hash tables inside the SpringBoot backend.) This will be replaced with a MondoDB database. The number of collections is to be determined.
2. A REST API backend written in Java/SpringBoot. The API may need to be redesigned, and the in-memory database needs to be strangled out and replaced with calls to the MongoDB backing store.
3. An Angular frontend that talks to the backend by making API calls. This may be replaced with a different language/framework. It will probably need to be rewritten as the backend API is redesigned.

This reworking is being done so that the example can be used to prepare CS-343 students for working with the microservices architecture of LibreFoodPantry.
